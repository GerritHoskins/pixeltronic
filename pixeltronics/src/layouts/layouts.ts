import CenteredLayout from './CenteredLayout.vue'
import DefaultLayout from './DefaultLayout.vue'

export default { CenteredLayout, DefaultLayout } as Record<string, any>
