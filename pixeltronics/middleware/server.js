// eslint-disable-next-line no-undef
const dotenv = require('dotenv')
// eslint-disable-next-line no-undef
const express = require('express')
// eslint-disable-next-line no-undef
const http = require('http')
// eslint-disable-next-line no-undef
const logger = require('morgan')
// eslint-disable-next-line no-undef
const path = require('path')
// eslint-disable-next-line no-undef
const router = require('./routes/index')
// eslint-disable-next-line no-undef
const { auth } = require('express-openid-connect')

dotenv.load()

const app = express()
// eslint-disable-next-line no-undef
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')

app.use(logger('dev'))
// eslint-disable-next-line no-undef
app.use(express.static(path.join(__dirname, 'public')))
app.use(express.json())

const config = {
  authRequired: false,
  auth0Logout: true,
  secret: '',
  clientID: '',
  baseURL: 'http://localhost:3000',
  issuerBaseURL: ''
}

// eslint-disable-next-line no-undef
const port = process.env.PORT || 3000
// eslint-disable-next-line no-undef
if (
  !config.baseURL &&
  // eslint-disable-next-line no-undef
  !process.env.BASE_URL &&
  // eslint-disable-next-line no-undef
  process.env.PORT &&
  // eslint-disable-next-line no-undef
  process.env.NODE_ENV !== 'production'
) {
  config.baseURL = `http://localhost:${port}`
}

app.use(auth(config))

// Middleware to make the `user` object available for all views
app.use(function (req, res, next) {
  res.locals.user = req.oidc.user
  next()
})

app.use('/', router)

// Catch 404 and forward to error handler
app.use(function (req, res, next) {
  const err = new Error('Not Found')
  err.status = 404
  next(err)
})

// Error handlers
app.use(function (err, req, res, next) {
  res.status(err.status || 500)
  res.render('error', {
    message: err.message,
    // eslint-disable-next-line no-undef
    error: process.env.NODE_ENV !== 'production' ? err : {}
  })
})

http.createServer(app).listen(port, () => {
  console.log(`Listening on ${config.baseURL}`)
})
