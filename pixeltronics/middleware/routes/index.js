// eslint-disable-next-line no-undef
var router = require('express').Router()

router.get('/', function (req, res, next) {
  res.render('index', {
    title: 'pixeltronic - login',
    isAuthenticated: req.oidc.isAuthenticated()
  })
})

// eslint-disable-next-line no-undef
module.exports = router
